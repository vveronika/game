module MyGameModule

  def MyGameModule.set_words
    return { "buuk": ["buku", "kubu"], "irot": ["roti"], "urmah": ["rumah", "harum", "murah", "umrah"]}
  end

  def MyGameModule.set_random_words
  	array_soal = []
  	hash_words = MyGameModule.set_words
		hash_words.each do |key, value|
		  array_soal << key.to_s
		end
		rand_words = array_soal.shuffle
  end

  def MyGameModule.lets_play(random_words)
  	point = 0
  	counter = 0 
  	hash_words = MyGameModule.set_words
  	while counter < hash_words.length
			tebakan = ""
			soal = random_words[counter]
			puts "Tebak kata: #{soal}"
			while !hash_words[soal.to_sym].include? tebakan
				puts "SALAH! silakan coba lagi" unless tebakan == ""
				puts "Jawab	: "
				tebakan = gets.chomp
		  end
		  point = point + 1
		  puts "BENAR score anda #{point} !"
		  counter = counter+1
		end
  end
end