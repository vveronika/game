gem 'test-unit'
require 'test/unit'
require 'test/unit/ui/console/testrunner'
require "./my_game_module.rb"

class GameTest < Test::Unit::TestCase  
  my_obj_test = MyGameModule.set_words

  def test_words()  	
    my_obj_test = MyGameModule.set_words     
  end

  test "my_obj_test should be a Hash" do
    assert_equal( my_obj_test.class, Hash)
  end

  test "random words should be an Array" do
    assert_equal( MyGameModule.set_random_words.class, Array )
  end

  test "random words should not be a Hash" do
    assert_not_equal( MyGameModule.set_random_words.class, Hash )
  end

  test "the first collection of words is [:buuk, ['buku', 'kubu']" do
    assert_equal( my_obj_test.first, [:buuk, ["buku", "kubu"]])    
  end

  test "the value of first words collection is 2 " do
    assert_equal( my_obj_test.first.length, 2)
  end

  test "the value words collection should not nil " do
    assert_not_nil(my_obj_test, nil)
  end

  test "the last words collection  is [:rumah, ['harum', 'murah', 'umrah']" do
    assert_equal( my_obj_test.values.last, ["rumah", "harum", "murah", "umrah"])
  end

  test "the length of words collection is 4" do
    assert_equal( my_obj_test.values.last.length, 4)
  end

  test "the length of randomed words collection is 3" do
    assert_equal( MyGameModule.set_random_words.length, 3)
  end

  test "the randomed words should not nil" do
    assert_not_equal( MyGameModule.set_random_words.last, nil )
  end
end
